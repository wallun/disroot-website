---
title: 'Cómo puedo ayudar'
bgcolor: '#FFF'
fontcolor: '#555'
---

### Realizar envío y seguimiento de errores
No importa si has estado utilizando **Disroot** desde hace tiempo o si recién te has unido, reportar errores es algo que siempre puedes hacer. Así que si descubres que algo no luce del todo bien o no funciona como debería, puedes informarnos de ello abriendo un tema en el [Tablero de proyecto de Disroot](https://board.disroot.org/project/disroot-disroot/issues).

### Envía características/funciones
También puedes usar el [tablero](https://board.disroot.org/project/disroot-disroot/issues) para hacernos saber qué nuevas características o funciones te gustaría ver en el futuro.

![](theme://images/logo_project_board.png?resize=35,35) [Tablero de proyecto](https://board.disroot.org/project/disroot-disroot/?classes=button5)

---

### Discute en el foro
Antes de enviarnos un correo con preguntas o problemas, busca en el [foro](https://forum.disroot.org/c/disroot) para ver si ya hay una respuesta allí. Si aún no puedes encontrar una solución a tu problema, publica una pregunta en la sección de **Soporte** en el foro. En la mayoría de los casos, otrxs Disrooters estarán disponibles para contestarte, lo cual es beneficioso para todos; probablemente recibirás una respuesta rápida, tendremos menos correos con los que lidiar y, lo más importante, más personas podrán ver el hilo cuando tengan un problema similar.

![](theme://images/logo_forum.png?resize=35,35) [Disroot Forum](https://forum.disroot.org/c/disroot?classes=button5)

### Ayuda a resolver los problemas de otras personas
Si estás usando **Disroot** desde hace tiempo, estás familiarizadx con la plataforma o has resuelto algunos problemas antes, entonces puedes ser de gran ayuda para otrxs que están en apuros intentado encontrarle la vuelta a todas las características. Es también una ayuda enorme para nosotrxs si más Disrooters colaboran respondiendo preguntas y solucionando problemas menores de otrxs, lo que implica que la carga de trabajo se reparta mejor y las respuestas puedan aparecer más rápidamente. Comienza mirando el foro de **Disroot** y nuestra **sala XMPP** para ver cómo puedes ayudar si otrx lo necesita.

![](theme://images/logo_chat.png?resize=35,35) [disroot@chat.disroot.org](xmpp:disroot@chat.disroot.org?classes=button5)

---

### Ayúdanos con las guías
Ya hemos escrito y traducido unas cuantas [guías y tutoriales](https://howto.disroot.org/es) para ayudar a lxs usuarixs a aprender cómo utilizar los servicios que proveemos, pero todavía queda un largo camino para cubrir todos los aspectos, para todos los dispositivos y sistemas operativos posibles. Necesitamos más gente involucrada en la redacción y traducción de las guías si queremos crear una documentación universal que pueda ser utilizada por nuestrxs usuarixs y lxs de otras plataformas que brindan servicios similares, y que estén también, en la medida de lo posible, en sus propios idiomas.

![](theme://images/logo_h2.png?resize=35,35) [Proyecto Howto de Disroot](https://howto.disroot.org?classes=button5)
![](theme://images/logo_chat.png?resize=35,35) [howto@chat.disroot.org](xmpp:howto@chat.disroot.org?classes=button5)
 

### Dona dinero
Y por último, pero no menos importante, el apoyo financiero siempre es necesario, así que no temas arrojar un maletín lleno de dólares en nuestro camino. Y si eso es mucho, entonces recuerda que cada centavo suma y que si decides "comprarnos un café" con frecuencia, eso sería grandioso. Para más detalles, mira [aquí](https://disroot.org/es/donate)

---
