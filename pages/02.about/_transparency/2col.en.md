---
title: Transparency
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

# Transparency and openness

![](about-trust.png)

---

<br>

At **Disroot** we use 100% free and open source software. This means that the source code (the way the software operates) is publicly accessible. Anyone is able to contribute improvements or adjustments to the software and it can be audited and controlled at any moment - no hidden back-doors or other malicious malware.

We want to be fully transparent and open towards people using our services and therefore we publish information about the current state of the project, the financial status, our plans and ideas. We would also like to hear your suggestions and feedback so we can deliver the best experience possible to all **Disrooters**.
