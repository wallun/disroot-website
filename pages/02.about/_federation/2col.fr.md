---
title: 'Fédération et Décentralisation'
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

# Fédération et Décentralisation

<br>

![](about-organize.jpg)

---

<br>

La plupart des services Internet sont gérés à partir de points centralisés appartenant à des entreprises et gérés par celles-ci. Ces derniers stockent les données privées de leurs utilisateurs et les analysent à l'aide d'algorithmes poussés afin de créer des profils précis de leurs "utilisateurs". Ces informations sont souvent utilisées pour exploiter les personnes dans l'intérêt des annonceurs. Les informations peuvent également être obtenues par des institutions gouvernementales ou par des pirates informatiques malveillants. Les données peuvent être supprimées sans avertissement pour des raisons douteuses ou en raison de politiques régionales de censure.

Un service décentralisé peut résider sur plusieurs machines, appartenant à différentes personnes, entreprises ou organisations. Avec les protocoles de fédération, ces instances peuvent interagir et former un réseau de plusieurs nœuds (= serveurs hébergeant des services similaires). On peut être capable d'arrêter un nœud mais jamais tout le réseau. Dans une telle installation, la censure est pratiquement impossible.

Pensez à la façon dont nous utilisons le courrier électronique; vous pouvez choisir n'importe quel fournisseur de services ou créer le vôtre, et toujours échanger des courriels avec des personnes utilisant un autre fournisseur de courrier électronique. Le courrier électronique repose sur un protocole décentralisé et fédéré.

Ensemble, ces deux principes fondent un vaste réseau qui repose sur une infrastructure assez simple et des coûts relativement bas. N'importe quelle machine peut devenir un serveur et participer à part égale au réseau. D'un petit ordinateur de bureau chez vous à un serveur dédié ou des racks avec plusieurs serveurs. Cette approche offre la possibilité de créer le plus grand réseau mondial détenu par les utilisateurs - exactement comme l'Internet était censé l'être.

Nous ne voulons pas que Disroot devienne une entité centralisée, mais plutôt qu'elle fasse partie d'une communauté plus vaste - un nœud parmi tant d'autres. Nous espérons que d'autres seront inspirés à créer plus de projets avec des intentions similaires.
