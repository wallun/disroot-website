---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Equipo de Disroot

Disroot fue fundada por **Antilopa** y **Muppeth** en 2015.
En Julio de 2019 **Fede** y **Meaz** se unieron al Equipo.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz")
