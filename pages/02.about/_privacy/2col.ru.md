---
title: privacy
fontcolor: '#555'
wider_column: left
bgcolor: '#fff'

---

<br>

Программное обеспечение может быть создано и сформированно во всё, что угодно. Каждая кнопка, каждый цвет и каждая ссылка, которую мы видим и используем в интернете, была помещена туда кем-то. Когда мы используем приложение, предоставленное нам, мы многого не видим, а иногда не задумываемся, сколько всего произошло за интерфейсом, который мы используем. Мы связываемся с другими людьми, храним файлы, организуем встречи и фестивали, отправляем почту или общаемся часами на пролёт, и всё это происходит магически.
В последние несколько десятков лет информация стала очень ценной, и её всё проще собирать и обрабатывать. Мы привыкли находиться под постоянным анализом, слепо принимать условия и соглашения для "своего же блага", доверяя властям и мульти-миллиардным компаниям защищать наши интересы, превращая всех нас в продукт их "человеческих ферм".

**Владей своими данными:**
Многие сети используют ваши данные, что бы зарабатывать деньги, анализируя ваши взаимодействия, и используют их для предоставления вам рекламы. **Disroot** не использует никаких ваших данных за исключением тех, которые нужны чтобы подключиться и использовать сервис.
Ваши файлы в облаке всегда зашифрованы вашим пользовательским паролем, а любой загруженный в **Lufi** файл так же зашифрован на стороне клиента; означая, что даже серверные администраторы не имеют доступа к вашим данным. Всякий раз, когда появляется возможность зашифровать что-то, мы её включаем; а если это не возможно, мы советуем использовать стороннее обеспечение для шифровки. Чем меньше мы, админы, знаем вашей информации - тем лучше :D. (Совет дня: *Никогда не теряйте свой пароль!*) [Просмотрите руководство](https://howto.disroot.org/ru/tutorials/user/account/ussc)

---

# Конфиденциальность


![](priv1.jpg?lightbox=1024)
![](priv2.jpg?lightbox=1024)
