---
title: Services
section_id: services
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
services:
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Crea e modifica documenti direttamente nel browser in collaborazione con altri e in modo sincrono."
        button: 'https://pad.disroot.org'
        buttontext: "Apri un pad"
    -
        title: Calc
        icon: calc.png
        link: 'https://disroot.org/services/pads'
        text: "Crea e modifica fogli di calcolo direttamente nel browser."
        button: 'https://calc.disroot.org'
        buttontext: "Apri un file Calc"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Paste Bin crittografata."
        button: 'https://bin.disroot.org'
        buttontext: "Condividi dei contenuti"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Condividi i tuoi file in modo temporaneo."
        button: 'https://upload.disroot.org'
        buttontext: "Condividi un file"
    -
        title: Polls
        icon: polls.png
        link: 'https://disroot.org/services/polls'
        text: "Servizio online per pianificare appuntamenti e prendere decisioni in modo facile."
        button: 'https://poll.disroot.org'
        buttontext: "Apri un poll"
    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Uno strumento di videoconferenza"
        button: 'https://calls.disroot.org'
        buttontext: "Inizia una videoconferenza"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Una applicazione per audio chat"
        button: 'https://mumble.disroot.org'
        buttontext: "Crea un canale"
        #badge: SPERIMENTALE
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Uno strumento alternativo e private oriented della famosa suite di Office."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Accedi"
        #badge: SPERIMENTALE
    -
        title: 'Games'
        icon: game.png
        link: 'https://disroot.org/en/quarantine'
        text: "Un game server dove divertirsi!"
        button: 'https://disroot.org/en/quarantine'
        buttontext: "Maggiori informazioni"
----
