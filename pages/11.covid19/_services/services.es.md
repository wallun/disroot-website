---
title: Servicios
section_id: services
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
services:
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Crea y edita documentos en tiempo real y colaborativamente desde el navegador"
        button: 'https://pad.disroot.org'
        buttontext: "Abre un pad"
    -
        title: Calc
        icon: calc.png
        link: 'https://disroot.org/services/pads'
        text: "Crea y edita planillas en tiempo real y colaborativamente desde el navegador"
        button: 'https://calc.disroot.org'
        buttontext: "Crea una planilla"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Pastebin/tablero de discusión en línea cifrado"
        button: 'https://bin.disroot.org'
        buttontext: "Comparte un pastebin"
    -
        title: Subida
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Software para alojar y compartir de manera cifrada y temporalmente archivos"
        button: 'https://upload.disroot.org'
        buttontext: "Comparte un archivo"
    -
        title: Encuestas
        icon: polls.png
        link: 'https://disroot.org/services/polls'
        text: "Servicio en línea para planificar eventos o tomar decisiones fácil y rápidamente"
        button: 'https://poll.disroot.org'
        buttontext: "Inicia una encuesta"
    -
        title: 'Llamadas'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Herramienta para video-conferencia"
        button: 'https://calls.disroot.org'
        buttontext: "Inicia un llamada"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Aplicación de audio chat con alta calidad de sonido y baja latencia"
        button: 'https://mumble.disroot.org'
        buttontext: "Crea un canal"
        #badge: EXPERIMENTAL
    -
        title: 'CryptPad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Alternativa privada-por-defecto a herramientas ofimáticas populares"
        button: 'https://cryptpad.disroot.org'
        buttontext: "Access"
        #badge: EXPERIMENTAL
    -
        title: 'Juegos'
        icon: game.png
        link: 'https://disroot.org/quarantine'
        text: "Un servidor de juegos para divertirnos un rato"
        button: 'https://disroot.org/quarantine'
        buttontext: "Más info"
----
