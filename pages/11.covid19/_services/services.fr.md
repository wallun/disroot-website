---
title: Services
section_id: services
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
services:
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Créez et modifiez des documents en collaboration et en temps réel."
        button: 'https://pad.disroot.org'
        buttontext: "Commencer"
    -
        title: Calc
        icon: calc.png
        link: 'https://disroot.org/services/pads'
        text: "Créez et modifiez des feuilles de calcul en collaboration et en temps réel."
        button: 'https://calc.disroot.org'
        buttontext: "Commencer"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Créez et modifiez des feuilles de calcul en collaboration et en temps réel."
        button: 'https://bin.disroot.org'
        buttontext: "Partager"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Logiciel d'hébergement et de partage de fichiers temporaires et chiffrés."
        button: 'https://upload.disroot.org'
        buttontext: "Partager"
    -
        title: Polls
        icon: polls.png
        link: 'https://disroot.org/services/polls'
        text: "Service en ligne pour planifier un rendez-vous ou prendre des décisions rapidement et facilement."
        button: 'https://poll.disroot.org'
        buttontext: "Sonder"
    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Un outil de vidéo conférence."
        button: 'https://calls.disroot.org'
        buttontext: "Appeler"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Une application de chat vocal à faible latence et de haute qualité."
        button: 'https://mumble.disroot.org'
        buttontext: "Créer une chaîne"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Une alternative privée aux outils de bureautique populaires."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Accès"
        #badge: EXPERIMENTAL
    -
        title: 'Games'
        icon: game.png
        link: 'https://disroot.org/en/quarantine'
        text: "Un serveur de jeu pour s'amuser!"
        button: 'https://disroot.org/en/quarantine'
        buttontext: "Plus d'info"
---
