---
title: Services
section_id: services
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
services:
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Create and edit documents collaboratively in real-time directly in the web browser."
        button: 'https://pad.disroot.org'
        buttontext: "Start a pad"
    -
        title: Calc
        icon: calc.png
        link: 'https://disroot.org/services/pads'
        text: "Create and edit spreadsheets collaboratively in real-time directly in the web browser."
        button: 'https://calc.disroot.org'
        buttontext: "Start a calc"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Encrypted online paste-bin/discussion board."
        button: 'https://bin.disroot.org'
        buttontext: "Share a pastebin"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Encrypted, temporary file hosting and sharing software."
        button: 'https://upload.disroot.org'
        buttontext: "Share a file"
    -
        title: Polls
        icon: polls.png
        link: 'https://disroot.org/services/polls'
        text: "Online service for planning an appointment or making decisions quickly and easily."
        button: 'https://poll.disroot.org'
        buttontext: "Start a poll"
    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "A videoconferencing tool."
        button: 'https://calls.disroot.org'
        buttontext: "Start a call"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "A low latency, high quality voice chat application."
        button: 'https://mumble.disroot.org'
        buttontext: "Create a channel"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "A private-by-design alternative to popular office tools."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Access"
        #badge: EXPERIMENTAL
    -
        title: 'Games'
        icon: game.png
        link: 'https://disroot.org/en/quarantine'
        text: "A game server to have some fun!"
        button: 'https://disroot.org/en/quarantine'
        buttontext: "More info"
----
