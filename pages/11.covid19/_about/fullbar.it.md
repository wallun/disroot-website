---
title: Covid-19 Kit
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Coronavirus - pandemia!
La pandemia legata al Coronavirus ci ha messo tutti in una situazione unica. Una larga maggioranza della popolazione mondiale deve rimanere nelle proprie case come misura per impedire che la malattia si diffonda ulteriormente. Ciò ha generato un collasso in una serie di servizi che dipendono da Internet. È per questo motivo che, qui in Disroot, abbiamo deciso di collaborare mettendo a disposizione alcuni strumenti utili in modo che tutti possano comunicare, lavorare e condividere. Abbiamo chiamato questi servizi il "Disroot Covid- 19 Kit".

## Questi servizi non richiedono registrazione.
Vogliamo aiutare altre organizzazioni o piccole aziende fornendo istanze private personalizzate ad un costo accessibile.
