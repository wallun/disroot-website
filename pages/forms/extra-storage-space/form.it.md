---
title: 'Spazio di archiviazione extra'
form:
    name: 'Spazio di archiviazione extra'
    fields:
        -
            name: username
            label: 'Nome utente'
            placeholder: 'Inserire il nome utente Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Indirizzo Email del contatto'
            placeholder: 'Inserisci un indirizzo Email per contattarti'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Amount of Space'
            label: 'Spazio di archiviazione totale'
            type: select
            default: 14GB
            options:
                14GB: '14GB (2.10€ mensili)'
                29GB: '29GB (4.35€ mensili)'
                54GB: '54GB (8.10€ mensili)'
            validate:
                required: true
        -
            name: commenti
            type: textarea
        -
            name: 'Dettagli del pagamento'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Dettagli del pagamento:**'
        -
            name: country
            label: 'Paese (necessario ai fini IVA)'
            placeholder: 'Si prega di specificare il paese in cui è registrato il tuo conto di pagamento'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Pagamento tramite (Può essere soggetto a spese di transazione extra)'
            placeholder: seleziona
            type: select
            options:
                paypal: Paypal
                bank: 'Bonifico bancario (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: frequency
            label: Mensile/Annuale
            type: radio
            default: yearly
            options:
                monthly: Mensile
                yearly: Annuale
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Invia
        -
            type: reset
            value: Azzera
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Richiesta di archiviazione extra'
                body: 'Gentile {{ form.value.username }}, <br><br> Abbiamo ricevuto una richiesta di spazio di archiviazione supplementare. <br><br>Dovresti ricevere la tua conferma via Email con il tuo riferimento di fatturazione. <br> Una volta ricevuto il tuo pagamento ti assegneremo il volume extra. <br><br> Grazie per sostenere Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'La tua richiesta è stata inviata!'
        -
            display: grazie
---

<h1 class="form-title"> Spazio di archiviazione extra </h1>
<p class="form-text">E' possibile estendere il cloud storage a 14, 29 o 54 GB al costo di 0,15 euro per GB al mese. È inoltre possibile scegliere di utilizzare parte di tutto lo spazio aggiuntivo per l'archiviazione della posta, si prega di specificarlo nella sezione commenti del modulo di richiesta.</p>
