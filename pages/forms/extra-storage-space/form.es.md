---
title: 'Espacio de Almacenamiento Extra'
form:
    name: 'Espacio de Almacenamiento Extra'
    fields:
        -
            name: Nombre de usuario
            label: 'Nombre de usuario'
            placeholder: 'Ingresa el nombre de usuario de Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: Correo
            label: 'Dirección de correo de Contacto'
            placeholder: 'Ingresa una dirección de correo para contactarte'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Cantidad de espacio'
            label: 'Espacio total de almacenamiento'
            type: select
            default: 14GB
            options:
                14GB: '14GB (2.10€ por mes)'
                29GB: '29GB (4.35€ por mes)'
                54GB: '54GB (8.10€ por mes)'
            validate:
                required: true
        -
            name: Comentarios
            type: textarea
        -
            name: 'Detalles del Pago'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Detalles del Pago:**'
        -
            name: País
            label: 'País (Necesario por razones de I.V.A)'
            placeholder: 'Por favor, especifica el país en el que está registrada tu cuenta de pago'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: Pago
            label: 'Pago a través de: (Podría estar sujeto a costos adicionales de transacción)'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                bank: 'Transferencia bancaria (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: Frecuencia
            label: Mensual/Anual
            type: radio
            default: yearly
            options:
                monthly: Mensual
                yearly: Mensual/Anual
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Enviar
        -
            type: reset
            value: Reiniciar
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Solicitud de Almacenamiento Extra'
                body: 'Estimad@ {{ form.value.username }}, <br><br> Hemos recibido una solicitud de espacio extra de almacenamiento. <br><br>Deberías recibir tu confirmación por correo con tu referencia de facturación. <br> Una vez que hayamos recibido tu pago, te adjudicaremos el espacio adicional. <br><br> ¡Gracias por apoyar a Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: '¡Tu solicitud ha sido enviada!'
        -
            display: thankyou
---

<h1 class="form-title"> Espacio de almacenamiento extra </h1>
<p class="form-text">Es posible ampliar tu almacenamiento en la nube a 14, 29 o 54 GB a 0.15 euro mensual por GB. También puedes elegir utilizar algo de todo el espacio adicional para almacenar correos. Por favor, especifica eso en la sección de comentario en el formulario de solicitud.</p>
