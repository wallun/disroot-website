---
title: 'Zusätzlicher Speicherplatz'
form:
    name: 'Extra Storage Space'
    fields:
        -
            name: username
            label: 'Benutzername'
            placeholder: 'Gib Deinen Disroot-Benutzernamen an'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Kontakt-Email-Addresse'
            placeholder: 'Gib Deine Kontakt-Email-Addresse an'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Amount of Space'
            label: 'Gesamt-Speicherplatz'
            type: select
            default: 14GB
            options:
                14GB: '14 GB (2.10 € pro Monat)'
                29GB: '29 GB (4.35 € pro Monat)'
                54GB: '54 GB (8.10 € pro Monat)'
            validate:
                required: true
        -
            name: comments
            label: Anmerkungen
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Zahlungsmodalitäten:**'
        -
            name: country
            label: 'Land (notwendig wegen VAT)'
            placeholder: 'Kontoheimat'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Bezahlung per (je nach Wahl fallen evtl. Gebühren an)'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                bank: 'Banküberweisung (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: frequency
            label: Zahlungsweise
            type: radio
            default: yearly
            options:
                monthly: monatlich
                yearly: Jährlich
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Absenden
        -
            type: reset
            value: Zurücksetzen
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Anfrage zusätzlicher Speicher'
                body: 'Hi {{ form.value.username }}, <br><br>wir haben Deine Anfrage auf zusätzlichen Speicherplatz erhalten. <br><br>Du solltest eine Bestätigung per Email erhalten, die auch die Zahlungsanweisungen enthält. <br>Wenn wir Deine Zahlung erhalten haben, werden wir Dir den zusätzlichen Speicherplatz zuweisen. <br><br>Vielen Dank für Deine Unterstützung von Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Deine Anfrage wurde gesendet!'
        -
            display: thankyou
---

# Zusätzlicher Speicherplatz

Es besteht die Möglichkeit, Deinen Cloud-Speicherplatz auf 14, 29 oder 54 GB zu erweitern für monatlich 0,15 €/GB. Du kannst Dich auch entscheiden, den zusätzlichen Speicherplatz teilweise oder vollständig für zusätzlichen Email-Speicherplatz zu nutzen. In dem Fall schreibe Deine Wünsche möglichst genau in den Kommentarbereich des Anfrageformulars.
