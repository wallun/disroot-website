---
title: 'Extra Storage Space'
form:
    name: 'Extra Storage Space'
    fields:
        -
            name: username
            label: 'User Name'
            placeholder: 'Enter the Disroot user name'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Contact Email Address'
            placeholder: 'Enter an email address to contact you'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Amount of Space'
            label: 'Total Storage Space'
            type: select
            default: 14GB
            options:
                14GB: '14GB (2.10€ per month)'
                29GB: '29GB (4.35€ per month)'
                54GB: '54GB (8.10€ per month)'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Payment details:**'
        -
            name: country
            label: 'Country (necessary for VAT purposes)'
            placeholder: 'Please specify the country your paying account is registered in'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Payment via (May be subjected to extra transaction fees)'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                bank: 'Bank transfer (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: frequency
            label: Monthly/Yearly
            type: radio
            default: yearly
            options:
                monthly: Monthly
                yearly: Yearly
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Extra Storage Request'
                body: 'Dear {{ form.value.username }}, <br><br> We have received a request for extra storage space. <br><br>You should receive your confirmation per E-mail with your billing reference. <br> Once we receive your payment we will assign you the extra storage. <br><br> Thank you for supporting Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Your request has been sent!'
        -
            display: thankyou
---

<h1 class="form-title"> Extra Storage Space </h1>
<p class="form-text">It is possible to extend your cloud storage to 14, 29 or 54 GB for the cost of 0.15 euro per GB per month. You can also choose to use some of all of the additional space for mail storage, please specify that in the comment section in the request form.</p>
