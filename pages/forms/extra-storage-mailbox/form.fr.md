---
title: 'Messagerie: Espace de stockage supplémentaire'
form:
    name: 'Messagerie: Espace de stockage supplémentaire'
    fields:
        -
            name: username
            label: "Nom d'utilisateur"
            placeholder: "Tapez votre nom d'utilisateur Disroot"
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: email
            label: 'Adresse email de contact'
            placeholder: 'Tapez une adresse email pour vous contacter'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Additional Inbox Storage'
            label: 'Esapce supplémentaire en GB'
            type: number
            validate:
              min: 1
              max: 10
              steps: 1
              value: 1
              required: true
        -
            name: comments
            type: textarea
        -
            name: 'Informations de paiement'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Informations de paiement:**'
        -
            name: country
            label: 'Pays (nécessaires à des fins de TVA)'
            placeholder: 'Veuillez spécifier le pays dans lequel votre compte de paiement est enregistré.'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Paiement par'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                bank: 'Virement bancaire (pays SEPA uniquement)'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Envoyer
        -
            type: reset
            value: Réinitialiser
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Demande de stockage supplémentaire'
                body: 'Cher/Chère {{ form.value.username }}, <br><br> Nous avons reçu une demande de stockage supplémentaire. <br><br>Vous devriez recevoir votre confirmation par E-mail avec votre référence de facturation. <br> Une fois que nous aurons reçu votre paiement, nous vous attribuerons le stockage supplémentaire. <br><br> Merci de soutenir Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Votre demande a été envoyée!'
        -
            display: Merci
---

<h1 class="form-title"> Espace de stockage supplémentaire </h1>

<div class="form-text">
<p markdown="1">Il est possible d'étendre la capacité de stockage de votre messagerie jusqu'à 10 Go pour le coût de 0,15 euro par Go et par mois, payé annuellement. Pour demander de l'espace de stockage supplémentaire pour votre messagerie et votre compte cloud, vous devez remplir ce [formulaire](https://disroot.org/en/forms/extra-storage-space)</p><br>
</div>
