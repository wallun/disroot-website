---
title: 'Zusätzlicher Postfachspeicher'
form:
    name: 'Zusätzlicher Postfach-Speicher'
    fields:
        -
            name: username
            label: 'Benutzername'
            placeholder: 'Gib Deinen Disroot-Benutzernamen an'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: email
            label: 'Kontakt-Emailadresse'
            placeholder: 'Gib Deine Kontakt-Emailadresse an'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-_@]*'
                required: true
        -
            name: 'Additional Inbox Storage'
            label: 'Zusätzlicher Speicher in GB'
            type: number
            classes: form-number
            outerclasses: form-outer
            validate:
              min: 1
              max: 10
              steps: 1
              value: 1
              required: true
        -
            name: comments
            label: Anmerkungen
            type: textarea
        -
            name: 'payment details'
            type: display
            size: large
            label: ' '
            markdown: true
            content: '**Zahlungsmodalitäten:**'
        -
            name: country
            label: 'Land (notwendig wegen VAT)'
            placeholder: 'Kontoheimat'
            type: text
            validate:
                pattern: '[A-Za-z]*'
                required: true
        -
            name: payment
            label: 'Bezahlung per (je nach Wahl fallen evtl. Gebühren an)'
            placeholder: select
            type: select
            options:
                paypal: Paypal
                bank: 'Banküberweisung (SEPA countries only)'
                faircoin: Faircoin
                bitcoin: Bitcoin
                patreon: Patreon
            validate:
                required: true
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Absenden
        -
            type: reset
            value: Zurücksetzen
    process:
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ form.value.email }}'
                subject: '[Disroot] Anfrage zu Extra-Speicher'
                body: 'Hi {{ form.value.username }}, <br><br>wir haben Deine Anfrage auf zusätzlichen Speicherplatz erhalten. <br><br>Du solltest eine Bestätigung per Email erhalten, die auch die Zahlungsanweisungen enthält. <br>Wenn wir Deine Zahlung erhalten haben, werden wir Dir den zusätzlichen Speicherplatz zuweisen. <br><br>Vielen Dank für Deine Unterstützung von Disroot!'
        -
            email:
                from: extra-storage@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.email }}'
                subject: '[Extra storage request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.html.twig'' %}'
        -
            message: 'Deine Anfrage wurde versendet!'
        -
            display: thankyou
---

# Zusätzlicher Speicherplatz

<div class="form-text">
<p markdown="1">
Es ist möglich, Deinen Email-Speicher von 1 GB auf bis zu 10 GB zu erweitern für monatlich 0,15 €/GB, jährlich zu zahlen. Um zusätzlichen Speicher sowohl für die CLoud als auch Dein Postfach anzufragen, benutze [dieses Formular](https://disroot.org/de/forms/extra-storage-space)</p><br>
</div>
