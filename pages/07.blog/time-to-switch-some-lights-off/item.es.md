---
title: 'Hora de apagar las luces'
date: '14-10-2020 10:00'
media_order: belarus.jpg
taxonomy:
  category: news
  tag: [disroot, noticias, matrix, diaspora, nextcloud]
body_classes: 'single single-post'

---

**Desenchufando Matrix y Diaspora\*.**

### Matrix

Como deben recordar, hace dos años decidimos no seguir brindando **Matrix** como servicio ([aquí el post](https://disroot.org/es/blog/matrix-closure)). La razón de esa decisión, aparte de una creciente, y aún no resuelta, preocupación por la privacidad, fue también la enorme cantidad de recursos necesarios del servidor para operar lo que es esencialmente una aplicación de chat basada en texto. Hemos decidido volver a las raíces y preferimos enfocarnos en brindar **XMPP** como nuestra solución de chat de referencia. Desde aquel anuncio hemos ido removiendo lentamente las cuentas inactivas. En este momento solo nos queda un puñado de usuarios y usuarias, pero aún así la cantidad de recursos que se utilizan es inaceptablemente grande: en este momento nuestro servidor de **Matrix** consume alegremente alrededor de 5GB de RAM (sin contar el uso de RAM de la base de datos), y 170GB de almacenamiento para la base de datos. Esto nos parece un tremendo desperdicio de recursos y por eso hemos decidido que es hora de apagar las luces. Y así, el **1° de diciembre desconectaremos nuestro servidor Matrix por completo**. Esperamos que aquellas personas que aún lo usan entiendan nuestra decisión y encuentren pronto un nuevo hogar en otro servidor.

### Diaspora*

Desde el [anuncio de la eliminación de nuestra instancia de **Diaspora\***](https://disroot.org/es/blog/demise-diaspora), hemos notado una gran caída en la cantidad de usuarios y usuarias activas de nuestro pod. Llegamos a tener solo unas pocas cuentas en los primeros dos meses. Por lo tanto, a diferencia de la eliminación gradual de **Matrix**, la decisión de apagar las luces de **Diaspora\*** maduró mucho más rápido. Al igual que **Matrix**, a partir del **1° de diciembre nuestro pod ya no será parte de la red federada**.

Una vez más, queremos agradecer a todas las personas involucradas durante los años que nos brindaron el software que fue una de las primeras fuentes de inspiración para comenzar **Disroot**. Tal vez algún día nos volvamos a encontrar en el multiverso federado.

# Nuevas aplicaciones de Nextcloud

Recientemente, hemos habilitado nuevas aplicaciones en **Nextcloud**, algunas de las cuales ustedes estaban solicitando desde hace rato. Estas son:

* **Formularios**: Sin demasiadas opciones para distraerse. Una aplicación directa y simple de encuestas y cuestionarios. Formularios permite exportar los resultados a un archivo CSV de forma similar a como lo hace Google Forms. <https://apps.nextcloud.com/apps/forms>
* **Citas**: Registra citas en tu calendario a través de un formulario seguro en línea. Las personas asistentes pueden confirmar o cancelar sus citas a través de un enlace de correo electrónico. <https://apps.nextcloud.com/apps/appointments>
* **Cospend**: Un gestor de presupuestos grupal/compartido. Puedes utilizarlo cuando compartes una casa, cuando vas de vacaciones con amigos o cada vez que compartas dinero con otras personas. <https://apps.nextcloud.com/apps/cospend>

Esperamos que les gusten las aplicaciones adicionales y las encuentren útiles en su vida diaria. Nosotros ya las estamos usando internamente y las estamos disfrutando bastante. Vendrán algunos tutoriales detallados, sin embargo si quieren darnos una mano, pueden unirse a la comunidad **Howto** en howto@chat.disroot.org


# Performance crítica de Nextcloud

Como probablemente habrán notado, las últimas semanas experimentamos algunos inconvenientes con ciertos servicios. Desafortunadamente, el origen de esto es **Nextcloud**. Parece que estamos chocando con algún problema del que aún no hemos encontrado la causa principal. Por ahora hemos movido los servicios afectados por la caída de rendimiento de **Nextcloud** a otra "caja", lo que significa que por lo menos esos funcionarán correctamente. En los próximos días intentaremos centrarnos en el problema y encontrar qué lo causa. Disculpen por esta turbulencia.
