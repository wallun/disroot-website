---
title: 'Nextcloud - problemi di performance'
date: '20-11-2019 15:00'
media_order: this-is-fine.0.jpg
taxonomy:
  category: news
  tag: [disroot, news, nextcloud]
body_classes: 'single single-post'

---

Come forse avrai notato, al momento la nostra istanza cloud è lenta e instabile. Stiamo collaborando con gli sviluppatori di Nextcloud e con alcune altre istanze interessate da quello che sembra essere un bug. L'obiettivo naturalmente è quello di trovare la causa principare di questo malfunzionamento.

Speriamo di risolvere al più presto questa situazione trovando una soluzione che ci permetta di ritornare ad una situazione di normalità. 

Purtroppo, fino ad ora, le notti insonni degli ultimi giorni non hanno prodotto alcun risultato.
