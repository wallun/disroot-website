---
title: 'Disroot user registration temporary closed'
media_order: dont_like_spam.jpg
published: true
date: '07-09-2018 15:10'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - registration
body_classes: 'single single-post'
---

When we started Disroot, we wanted to have a bunch of privacy respecting tools and share them with other people, with a community of those who may share our ethical or technical choices. We had put a lot of our time and effort to build this platform and give the best and useful free and open source solutions we could find, sharing our experience and knowledge in the process. That’s the Disroot essence.

Since a year ago, we’re experiencing a non-stop growing problem with our email service. Now we have a huge amount of abuse accounts stealing our time and creating a lot of troubles every single day. That means we dedicated significant chunk of our lives trying to solve this issue instead of improving the services. Currently we bet that about 80% of our user base are scammers, spammers and abusers of all sort. Most of our energy goes into fighting these people and we are continuously maintaining a platform that is being abused by people with ill intentions. We know that as a result of our recent popularity we should expect a certain percentage of shady accounts, but in the current situation this percentage has become overwhelming with a number far exceeding the credible Disrooters we want to support. This is not only an irritant for us but a threat to the credibility of the platform and therefore to all who are using it trustfully.

This means we need to find a solution to this problem. For now we've decided to close registrations as a protection measure for our users and Disroot itself while we are trying to think of proper ways to solve it.

In coming days we will be busy cleaning up our user-base from all the abusive "pest" accounts and we will plan out how to go about user registration in the future. We want to maintain the model where onboarding is as easy as can be and where anyone could join the Disroot community and feel right at home. At the same time we want to prevent malicious behavior from ruining it for all of us. It's a hard nut to crack but we hope to find a solution that will satisfy most of us.
