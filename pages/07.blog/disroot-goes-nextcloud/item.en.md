---
title: 'Disroot goes Nextcloud'
date: 07/26/2016
taxonomy:
  category: news
  tag: [disroot, news, nextcloud]
body_classes: 'single single-post'

---


Some weeks ago the open source community around ownCloud was surprised to hear many of the core developers, including one of the founders have decided to part ways with ownCloud inc. Frank Karlitschek announced it first on his blog ([http://karlitschek.de/2016/04/big-changes-i-am-leaving-owncloud-inc-today/](http://karlitschek.de/2016/04/big-changes-i-am-leaving-owncloud-inc-today/)). Naturally, this action left a lot of question marks regarding the future of the project. Many were concerned ownCloud will now focus more on the enterprise product and will therefor neglect the community‘s open source version. Soon after Karlitchek’s announcement, ownCloud Inc. announced their “ownCloud Foundation” project which intended to address such concerns. However, as pointed by several people in the community, this move did not address the issues that were the cause of staff‘s exodus (Contribution licensing among others) and rather it was seen by big part of the community as a move to keep things as they are while saving face.


We didn’t have to wait long to hear a new fork of ownCloud has been created under the name Nextcloud, and to no surprise, Frank Karlitschek and all the core developers that previously left ownCloud inc. were behind it. Nextcloud in contrary to ownCloud wants to be community oriented. With no restrictions in licensing (link to what CLA is) and with no enterprise version. They want to remain as transparent as possible and to base their business model on enterprise hosting and support rather than premium functionalities, just like many open source projects do these days.


This reassuring words from the guys at Nextcloud ([http://karlitschek.de/2016/06/nextcloud/](http://karlitschek.de/2016/06/nextcloud/)) made us decide to jump ship and enjoy more freedom. Therefor we would like to announce we have begun testing Nextcloud for disroot. As soon as we make sure all the applications hosted on our platform are compatible with Nextcloud, we will announce the exact date we would like to switch.


For all you Disrooters out there, there will be no significant difference in the short run. The Nextcloud release at the moment is no different to ownCloud, and everything should work out of the box. Even though Nextcloud has already released their mobile versions of the apps and the desktop sync client (which of course we would recommend you to move to once we make the switch), the ownCloud mobile apps are currently compatible. We do expect significant improvement to the user experience in the long run. The community around Nextcloud has grown like mushrooms over the last few weeks and we are very positive it will keep on growing. That  will naturally result in better user experience, more apps, more integrations and all the goodies you would expect form your personal cloud.
