---
title: 'Happy New Year!'
media_order: vlcsnap-2017-12-24-14h24m37s984.png
published: true
date: '24-12-2017 16:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
body_classes: 'single single-post'
---

We hope you are all enjoying the last days of 2017 and are ready to take on what comes next year.
These last months have been really encouraging for us with all the positive feedback we've been getting, the increase of support and of course the impressive contribution of translations.
We are now ready to introduce the couple of extra feature we've been promising for some time - The possibility to link your own domain to use for email aliases and the possibility to add extra space to your cloud storage.

### Custom Domain linking for email
As we mentioned last announcement, custom domain linking is now possible. This means you can now use @yourdomain.net to send and receive emails. Just like with email aliases, this feature is available to anyone who decides to help the project financially with a monthly "cup o' coffee". If you are interested in getting your domain linked with your Disroot account, have a look at [this form](https://disroot.org/en/forms/domain-linking-form).

### Extra disk space prices
A lot of people have been asking and are waiting for the details regarding extra cloud space. Last week we've sat together and came up with our price per GB. Considering the hardware, backup and capacity costs our price per GB is set to 0.15euro. We will at first offer packages of 14GB, 29GB, 54GB (total space). Once we upgrade our file storage server we will be able to offer even larger storage.
We still need to consult and create the proper procedure of giving out extra space (since this is not donation based it has to follow other administration rules etc), so request forms are not available yet. We will focus on that in first weeks of January so keep your ears and eyes open for announcements.

### Translations
We were very excited when several Disrooters started approaching us offering to translate our howto's. We are even more excited now after we have seen with what motivation and enthusiasm it has been done. In just a couple of weeks nearly all the tutorials on [howto.disroot.org](https://howto.disroot.org) have been translated to Spanish, Portuguese and French and big part of the [disroot.org](https://disroot.org) website is already translated into French.
We cant find the words to express our gratitude and we hope all you guys will enjoy all the tutorials translated to your native languages. So Fede, mdrouet, maryjane, track41 and all the others who helped translating, you guys are amazing! Thank you very much for all the hard work you put into the project.

If you want to help these guys or would like to translate stuff into yet another language, please join the [#disroot_howto:disroot.org matrix channel](https://chat.disroot.org/#/room/#disroot_howto:disroot.org).

#### With this last short announcement of the year we would also like to wish you all a happy new year, lots of luck and joy with whatever you chose to do in the upcoming 2018.
####
