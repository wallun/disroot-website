---
title: 'Volvamos al trabajo'
date: '04-09-2019 11:30'
media_order: coffee.jpg
taxonomy:
  category: news
  tag: [disroot, news, anniversary, birthday]
body_classes: 'single single-post'

---
*(Sprint 43)*

Hola. ¿Descansaron bien? ¿Comieron buena comida, visitaron lugares? Muy bien... Ahora, ¡de vuelta al trabajo!

Las semanas que pasaron, como escribimos en las entradas previas del blog, estuvieron llenas de trabajo. Al punto que nos dimos cuenta que estábamos cargando demasiado sobre nuestros hombros, lo que significa que tendemos a empezar cosas pero no terminarlas a tiempo. Ahora, lentamente estamos moviéndonos para terminar lo que previamente comenzamos, así que aquí va un pequeño bote de cosas en las que hemos estado ocupados recientemente. Ahhh, y no se preocupen, tuvimos algún tiempo de ocio también.

# Nuevas traducciones

En semanas pasadas testeamos varios programas de traducción, pero no pudimos encontrar alguno que cumpliera con nuestros requerimientos. Los sitios web y la documentación, a diferencia de las aplicaciones, son más difíciles de traducir utilizando un software dedicado y hay que dar muchas vueltas para mejorar muy poco. Así que decidimos continuar el trabajo de traducción a través de **Git**. Para algunos puede llegar a significar un nivel inicial más alto, pero aprender lo básico de **Git** y **Markdown** son buenas habilidades para adquirir.

Así que empezamos a implementar un nuevo procedimiento para crear, editar y traducir las guías y tutoriales. También creamos un muy buen resumen del estado actual de las traducciones, un archivo README para el repositorio de **Howto** y un tablero de **Taiga** donde incluso muchas personas trabajando en el mismo idioma puede coordinar sus trabajos. Hasta ahora funciona muy bien. Todavía estamos haciendo ajustes, refinando la documentación y mejorando el proceso, pero estamos en la dirección correcta para tener y brindar todas las guías y tutoriales en la mayor cantidad posible de idiomas. Puedes ver el **Estado de Traducciones de las Guías** [aquí](https://howto.disroot.org/en/contribute/translations_procedure/state/Translations_Howto.pdf) y el **Estado de Traducciones del Sitio** [aquí](<https://howto.disroot.org/en/contribute/translations>_procedure/state/Translations_Website.pdf).

En las últimas semanas comenzamos a traducir el sitio y algunas guías en **ruso** y **alemán**, y hacer revisiones y actualizaciones en **francés**, **italiano** y **español**. El mayor crédito de este progreso corresponde a los disrooters **shadowsword**, **wisbit, l3o** y **keyfear**, por involucrarse tanto y contribuir no solo con las traducciones, sino también a través de las devoluciones, comentarios y actitud colaborativa.

**Fede** ha sido una hormiga ocupada coordinando todo el trabajo así como también fusionando, verificando y aprobando los cambios y traducciones. También ha estado trabajando en las mejoras de los procedimientos y en la visión general.

Si quieres ponerte en contacto con el equipo y ayudar con las traducciones o las guías, únete a nuestra sala XMPP: howto@chat.disroot.org

[Tablero de las traducciones](https://board.disroot.org/project/fede-disroot-translations/epics)<br>

[Tablero de las guías](https://board.disroot.org/project/disroot-disroot-h2/epics)

# Nuevo tema para Howto

En las últimas semanas **Antilopa** estuvo ocupada creando el nuevo tema para [https://howto.disroot.org](https://howto.disroot.org). Hace un tiempo notamos que el anterior tema no se ajustaba a nuestras necesidades, haciendo que la información se volviera difícil de encontrar a medida que el número de tutoriales crecía. El resultado, como siempre, es bastante genial. **Antilopa** hizo un muy buen tema, minimalista y limpio que se parece al estilo de nuestro sitio web. Todavía hay algunos problemas que han sido reportados sobre la manera en que se visualiza el nuevo sitio en algunas pantallas y **Antilopa** está trabajando para arreglarlo. Si encuentras algo, por favor, repórtalo en nuestro **tablero de problemas** [aquí](<https://board.disroot.org/project/disroot-disroot/issues>). Recuerda agregar el prefijo [WEBSITE] así puede ser encontrado más rápido y arreglado.

# Encontrando la vuelta a algunos procedimientos internos

**Meaz** estuvo ocupado recorriendo la cola de nuestro sistema de tickets de soporte que se ha vuelto un desorden, como el de un cuarto de estudiantes después de una larga semana de fiesta. El resultado de esto es una mejorada y más clara cola de soporte, así como la introducción de turnos. Cada semana, una persona distinta es responsable de asignar los tickets, dar seguimiento a los que ingresan y también patear el culo de aquellos que holgazanean demasiado con las respuestas (guiño, guiño... ustedes saben quiénes son...).

**Fede** diseñó un buen documento de bienvenida que pondremos pronto en algún lugar en el sitio. Debería ayudar en los primeros pasos a l@s nuev@s usuari@s. También hemos comenzado una larga discusión acerca de las cosas que podemos mejorar para que puedan encontrarse en casa fácilmente. Sabemos que hay muchos bordes que pulir por todos lados. Nuestra meta es trabajar sobre una experiencia más unificada a través de todos los servicios. Hemos pasado algún tiempo pensando y definiendo algunas de las más accesibles y llegaremos a la implementación en los próximos sprints.

# FAQ (Preguntas frecuentes)

Sí, finalmente. No más responder *'usa solo tu nombre de usuario y no nombredeusuario@disroot.org'* por centésima vez cada día :P Gracias a **Meaz**, finalmente hemos comenzado y compilado un **FAQ** (Preguntas Frecuentes) en el sitio web **\o/**. Además, ha comenzado también a enseñarle a nuestro bot **milkman** (el nombre está sujeto a cambio, el chiste ya no tiene gracia), a responder preguntas frecuentes para todos aquellos que estén en el chat. **Antilopa** seguirá trabajando en las mejoras visuales de la página y en la experiencia del usuario y, por supuesto, seguiremos agregando más preguntas y aún más respuestas mientras avanzamos.

# Pastilla Roja
![](redpill_0.png)

**RedPill** es nuestra versión del webchat **XMPP** de **Opcode** llamado **ConverseJS**. En las últimas semanas **Muppeth**, como proyecto paralelo, decidió trabajar en la apariencia del webchat. El resultado lo pueden ver en [https://webchat.disroot.org]((https://webchat.disroot.org). Este es solo el comienzo y es un cambio largamente esperado. En un futuro cercano, estaremos ocupados implementando varias características nuevas como conferencia de audio/video, mensajes de voz, reacciones y también muchas mejoras visuales. Este es un primer avance. El nombre para nuestro "sabor" del cliente fue una idea de último minuto, ya que el color principal y dominante es el azul en lugar del rojo, pero veremos cómo funcionan los rojos en esta configuración. Para aquellos que quieran seguir el trabajo sobre el webchat, pueden usar https://devchat.disroot.org, donde jugamos y probamos ideas. El código fuente de nuestros cambios será publicado tan pronto como hayamos migrado nuestros repositorios git actuales a una nueva casa.

Aquí tienen algunas capturas:
![](redpill_1.png)

![](redpill_2.png)

# El add-on (accesorio) de Diaspora\* en Hubzilla

...Y tiempo de malas noticias. A pesar de las incontables horas depurando errores en nuestro conector de **Hubzilla** a la red **Diaspora***, no hemos hecho ningún progreso en términos de encontrar una solución. Actualmente (de hecho, así ha sido durante meses) el add-on de **Diaspora*** está deshabilitado. Hemos decidido por ahora no invertir más recursos buscando activamente soluciones. Sin embargo, continuaremos reportando a los desarrolladores principales de **Hubzilla**. Esperamos que en el futuro un error sea identificado (si no por nosotros, entonces quizá alguien más) y ahora, en vez de eso, vamos a enfocarnos en preparar **Hubzilla** de manera que podamos sacarla de nuestro testeo beta abierto y ubicarla en el sitio web como la red social a seguir.

Como pueden ver, había bastantes cosas. No se preocupen, las siguientes entradas serán mucho más cortas ya que ahora somos más estrictos respecto a la cantidad de trabajo del que nos hacemos cargo. :P
