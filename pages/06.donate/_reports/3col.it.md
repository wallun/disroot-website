---
title: 'Annual reports'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

#Report annuale:

**Annual Report 2019**
[Report 2019](/annual_reports/AnnualReport2019.pdf?target=_blank)


**Report annuale 2018** <br>
[Report 2018](https://disroot.org/annual_reports/AnnualReport2018.pdf)


**Agosto 2016 - Agosot 2017** <br>
Costi: €1707.82 <br>
Donazioni: €1380.45 <br>
[Report 2017](https://disroot.org/annual_reports/2017.pdf)

<br>

**2015 - Agosto 2016** <br>
Costi: €1569.76 <br>
Donazioni: €264.52

---



---
<br>
# Donazioni hardware
Nel caso dovessi avere a disposzione delle componenti hardware utili al progetto (memorie, server, CPU, harddrive, raid/network card ecc.), contattaci. Ogni componente è la benvenuta. Le componenti che non possiamo utilizzare possiamo venderle per incassare denaro da destinare al progetto.
