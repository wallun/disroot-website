---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Siamo riconoscenti per il supporto!

Con il tuo aiuto ci stiamo avvicinando al raggiungimento del nostro obiettivo di sostenibilità finanziaria. Stiamo dimostrando che un modello economico e sociale altro è possibile. 
