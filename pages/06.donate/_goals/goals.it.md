---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Condividiamo la nostra fortuna'
        text: "Se riceviamo almeno 400 EUR in donazioni, condividiamo il 15% del nostro surplus con gli sviluppatori del software che stiamo utilizzando. Disroot non esisterebbe senza quegli sviluppatori. "
        unlock: yes
    -
        title: 'Paga una quota di volontario'
        text: "Se alla fine del mese ci rimangono più di 140 €, paghiamo a un membro di Disroot Core una quota di volontariato di 140 €. "
        unlock: yes
    -
        title: 'Paga due quote ai volontari'
        text: "Se alla fine del mese ci rimangono più di 280 €, paghiamo a due membri di Disroot Core una quota di volontariato di 140 €. "
        unlock: yes
    -
        title: 'Paga tre quote ai volontari '
        text: "Se alla fine del mese ci rimangono più di 420 €, paghiamo a tre membri di Disroot Core una quota di volontariato di 140 €. "
        unlock: no
    -
        title: 'Paga quattro quote ai volontari'
        text: "Se alla fine del mese ne rimangono più di 560, paghiamo a quattro membri di Disroot Core una quota di volontariato di 140 €. "
        unlock: no
---

<div class=goals markdown=1>

</div>
