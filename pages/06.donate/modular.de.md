---
title: Spenden
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _donate
            - _overview
            - _goals
            - _reports
body_classes: modular
header_image: donate-banner.jpeg
---
