---
title: 'Recompensas'
bgcolor: '#f2f2f2ff'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: "Donando 30€"
        text: "Obtienes una tarjeta personalizada de saludos de Año Nuevo ."
    -
        title: "Donando 75€"
        text: "Una sorpresa especial de Año Nuevo rumbo a tu buzón."
    -
        title: "Donando 100€"
        text: "Eliges una nueva imagen para la página principal de Disroot.org por dos semanas.*"
    -
        title: "Donando 500€"
        text: "Seleccionas un nuevo nombre de dominio para un alias personalizado de Disroot que será otorgado a todxs lxs Disrooters.*"


---

<div markdown=1>
`*` Disroot.org se reserva el derecho de denegar la recompensa si la imagen o el nombre violan nuestros <a href="/tos"> TdS</a>
</div>
