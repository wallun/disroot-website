---
title: 'Cómo se usa Lufi'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## ¿Cómo funciona?

Arrastra y suelta archivos en el área correspondiente, o utiliza la manera tradicional para navegar y seleccionarlos, y estos serán cargados, cifrados y enviados al servidor. Obtendrás dos links por archivo: un link de descarga, que le das a las personas con las que quieres compartir el archivo, y un link de eliminación, que te permite borrar el archivo cuando quieras.
Puedes ver la lista de tus archivos haciendo click en el link "Mis archivos" en la parte superior derecha de esa página.

No necesitas registrarte para subir archivos.

---

![](en/lufi-drop.png?lightbox=1024)
