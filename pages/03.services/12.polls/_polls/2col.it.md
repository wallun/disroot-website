---
title: Sondaggio
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://poll.disroot.org/">Crea un sondaggio</a>

---

![](framadate_logo.png)

Framadate è un servizio online per pianificare un appuntamento o prendere una decisione in modo semplice e veloce.
Non è richiesta alcuna registrazione.

[https://poll.disroot.org](https://poll.disroot.org)
Pagina iniziale del progetto: [https://git.framasoft.org/framasoft/framadate](https://git.framasoft.org/framasoft/framadate)
