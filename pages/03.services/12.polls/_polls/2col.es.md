---
title: Encuestas
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://poll.disroot.org/">Crear una encuesta</a>

---

![](framadate_logo.png)

Las Encuestas de **Disroot** están desarrolladas por **Framadate**, que es un servicio en línea para planificar un encuentro o tomar una decisión de manera sencilla y rápida. ¡Crea tu encuesta, compártela con tus amigos, amigas o colegas así pueden participar en el proceso de decisión y obtener los resultados!

No necesitas una cuenta de **Disroot** para utilizar este servicio.

Encuestas Disroot: [https://poll.disroot.org](https://poll.disroot.org)

Código fuente: [https://git.framasoft.org/framasoft/framadate](https://git.framasoft.org/framasoft/framadate)
