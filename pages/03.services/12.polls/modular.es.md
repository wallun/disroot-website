---
title: Encuestas
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _polls
            - _poll-highlights
            - _poll-create
            - _poll-share
            - _poll-participate
            - _poll-results
            - _empty-bar
body_classes: modular
header_image: 'poll-banner.jpg'
---
