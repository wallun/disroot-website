---
title: 'Xmpp Chatrooms'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Chat-rooms

With XMPP your can create and join private or public rooms in the network. Rooms address are simple to remember and resemble email mailinglists like: *your_room*@chat.disroot.org. Additionally you can set various privacy settings per room such as: Whether the room history should be stored on the server, whether the room is publicly searchable and open for anyone to join, you can set admins and moderators for the room and room encryption.  

Check the [global room list](https://search.jabbercat.org/rooms/1) of all the servers that voluntarily submit their public rooms to global directory.


---

## <br> ##
![](chatrooms.png)
