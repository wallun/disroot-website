---
title: 'Bots XMPP'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Bots

Los bots son cuentas de chat que pueden hacer cosas por ti. Desde chequear el clima, saludar a las personas recién llegadas, recordar cosas por ti, incluso realizar búsquedas en la web o proporcionarte fuentes RSS y una tonelada de cosas útiles y otras más inútiles. Ofrecemos un número de bots multipropósito basados en **hubot** así como también un servidor de bots dedicado llamado `bot.disroot.org`. Si tienes un bot y estás buscándole un hogar, comunicate con nosotros.<br>

Nuestro servidor de bots no guarda ningún historial en él.

---

![](hubot_logo.png)
