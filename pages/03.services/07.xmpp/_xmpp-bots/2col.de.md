---
title: 'Xmpp Bots'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Bots

Bots sind Chat-Accounts, die wie kleine Helferlein Dinge für Dich erledigen können, ob das Abrufen des aktuellen Wetters am angefragten Ort, das Begrüßen von Neuankömmlingen, Dich an Sachen zu erinnern und sogar Web-Suchen durchzuführen oder Dich mit RSS-Feeds zu versorgen und tonnenweise nützliche und nutzlose Dinge. Wir bieten einige auf **hubot** basierende, vielseitige Bots an, ebenso wie einen dedizierten Bot-Server unter `bot.disroot.org`. Wenn Du einen Bot hast und nach einem Zuhause für ihn suchst, sprich mit uns. <br>

Unser Bot-Server speichert keinen Verlauf.

---

![](hubot_logo.png)
