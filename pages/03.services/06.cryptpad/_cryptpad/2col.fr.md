---
title: Cryptpad
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://cryptpad.disroot.org/register/">S'enregistrer</a>
<a class="button button1" href="https://cryptpad.disroot.org/">Créer des Pads</a>

---
![](cryptpad_logo.png)

## Cryptpad
Le Cryptpad de Disroot est propulsé par le Cryptpad. Il fournit une suite bureautique collaborative totalement chiffrée de bout en bout. Il vous permet de créer, de partager et de travailler ensemble sur des documents texte, des feuilles de calcul, des présentations, des tableaux blancs ou d'organiser votre projet sur un tableau kanban. Tout cela sans laisser d'informations sur les fichiers puisqu'ils sont chiffrées avant qu'elles ne quittent votre ordinateur.

Cryptpad Disroot: [cryptpad.disroot.org](https://cryptpad.disroot.org)

Page d'accueil du projet: [https://cryptpad.fr](https://cryptpad.fr)

Code source: [https://github.com/xwiki-labs/cryptpad](https://github.com/xwiki-labs/cryptpad)
