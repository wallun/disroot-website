---
title: 'Stockage de fichiers'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](drive.png)

## Stockage de fichiers chiffrés
Téléchargez et partagez n'importe quel fichier. Tous les fichiers stockés sont chiffrés de bout en bout !



---
![](code.png)

## Éditeur de code collaboratif
Modifiez votre code avec les membres de votre équipe tout en le chiffrant de bout en bout, avec aucune connaissance de vos informations côté serveur.
