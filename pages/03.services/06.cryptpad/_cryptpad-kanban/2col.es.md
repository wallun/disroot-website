---
title: 'Kanban'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](todo.png)

## Lista simple de Pendientes y tablero de Kanban cifrados
Crea fácilmente, comparte y gestiona tu Lista de Pendientes y tus tableros de [Kanban](https://es.wikipedia.org/wiki/Kanban_(desarrollo)) cifrados.


---
![](whiteboard.png)

## Pizarra
Pizarras cifradas que te permiten dibujar en tiempo real junto a otras personas. Todo está cifrado de extremo-a-extremo.
