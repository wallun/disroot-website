---
title: 'Link personalizado'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](link.png)

---

# Link personalizado
Puedes elegir tu propio nombre de sala de conferencia, que será su dirección.
