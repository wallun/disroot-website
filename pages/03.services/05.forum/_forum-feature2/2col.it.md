---
title: 'Caratteristiche - 2'
wider_column: left
---

# Intefraccia utente:
L'interfaccia di Discourse non è solo bella, è anche pratica:

- Le notifiche dinamiche vi informano quando qualcuno risponde ad un vostro post, vi quota o mensiona il vostro nome. Nel caso non foste online la notifica avverà attraverso un messaggio di posta elettronica.
 - Lunghe discussioni non saranno suddivise su più pagine, ma verranno continuamente mostrate durante la lettura
 - Rispondi mentre stai leggendo un messaggio. Con la suddivisione della pagina in più aree, puoi rispondere ai messaggi e dinamicamente aggiungere citazioni o link.
 - Aggiornamenti in tempo reale. Se un topic viene modificato o altre risposte vengono aggiunge mentre stai leggendo o rispondendo, la pagina si aggiorna automaticamente.


# Ricerca
È presente uno strumento di ricera molto potente. Propone i risultati in tempo reale e offre strumenti di ricerca avanzati.

---

![](en/discourse_feature2.png?lightbox=1024)
