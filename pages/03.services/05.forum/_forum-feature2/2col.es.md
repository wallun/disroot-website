---
title: 'Características 2'
wider_column: left
---

# Una atractiva interfaz de usuario:
La interfaz de usuario es muy práctica:

<ul class=disc>
<li>Las notificaciones dinámicas te darán avisos cuando alguien responda a tus publicaciones, te cite o mencione tu nombre, y si no estás en línea, recibirás una notificación por correo.</li>
<li>No más discusiones que están divididas en páginas, el hilo aparece a medida que te desplazas.</li>
<li>Responde mientras lees. Con la pantalla dividida puedes leer otras respuestas o hilos, y agregar de manera dinámica citas, vínculos y referencias.</li>
<li>Actualizaciones en tiempo real. Si un tópico cambia mientras lo estás leyendo o respondiendo, o llegan nuevas respuestas, se actualizará automáticamente.</li>
<li>Busca desde cualquier página, solo comienza a tipear y consigue los resultados instantáneamente mientras lo haces.</li>

---

![](en/discourse_feature2.png?lightbox=1024)
