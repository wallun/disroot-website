---
title: Discourse
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Sign up for Disroot</a>
<a class="button button1" href="https://forum.disroot.org/">Log in or Signup for forum only account</a>

---
![](discourse_logo.png)

## A Modern approach to discussion forums.

Disroot's forum is powered by **Discourse**. Discourse is a fully open-source modern approach to discussion forums. It offers everything your community, group or collective needs to create their communication platform, both public and private. It also gives possibility for individuals to create topics on subjects of their interest and find others to discuss it with, or simply join already existing community.

Disroot Forum: [https://forum.disroot.org](https://forum.disroot.org)

Project Homepage: [http://discourse.org](http://discourse.org)

Source code: [https://github.com/discourse/discourse](https://github.com/discourse/discourse)
