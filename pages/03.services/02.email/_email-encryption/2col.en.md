---
title: 'Email Encryption'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## What is encryption

---

Encryption is when you change data with a special encoding process so that the data becomes unrecognizable (it's encrypted).  You can then apply a special decoding process and you will get the original data back. By keeping the decoding process a secret, nobody else can recover the original data from the encrypted data.

[Video - How asymmetric encryption works](https://www.youtube.com/watch?v=E5FEqGYLL0o)
