---
title: 'Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
Puedes acceder a tu correo electrónico desde cualquier dispositivo, utilizando nuestro cliente web en [https://mail.disroot.org](https://mail.disroot.org)

Nuestro webmail está desarrollado por **RainLoop**. Es simple, rápido y proporciona la mayoría de las características que podríamos esperar del correo web:

<ul class=disc>
<li>Temas: Selecciona tu imagen de fondo.</li>
<li>Hilos: Muestra las conversaciones en forma de hilos jerárquicos.</li>
<li>Traducciones: La interfaz está siendo traducida a muchos idiomas</li>
<li>Filtros: Configura reglas para mover o copiar correos automáticamente a carpetas específicas, reenviarlos o rechazarlos.</li>
<li>Carpetas: Gestiona tus carpetas (agregar, quitar u ocultar).</li>
<li>Cifrado GPG: El cliente web <b>RainLoop</b> ofrece una opción de cifrado incorporado.</li>
<li>Contactos: puedes agregar tus contactos e incluso vincular los de Nextcloud directamente en el webmail.</li>
</ul>

**AVISO**: *Por favor, ten en cuenta que se trata de cifrado del lado del servidor, lo que significa que no tienes control sobre tu clave secreta. Esto no es tan seguro como utilizar un cliente de correo de escritorio, como Thunderbird, con cifrado local. Pero, por otro lado, es mejor que no usar cifrado en absoluto...*

Página del proyecto: [https://www.rainloop.net](https://www.rainloop.net/)
