---
title: 'Messagerie Web'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Messagerie Web:

![Messagerie Web](en/webmail.png)

---

<br>
Vous pouvez accéder à votre messagerie de n'importe quel appareil en utilisant la messagerie web à l'adresse: [https://mail.disroot.org](https://mail.disroot.org)

Notre webmail est propulsé par **RainLoop**. RainLoop est une approche moderne de la messagerie Web. Cela a l'air nouveau et simple, mais cela vous offre la plupart des fonctionnalités que l'on peut attendre de la messagerie Web:

<ul class=disc>
<li>Thèmes: Sélectionnez votre photo de fond.</li>
<li>Fils: Afficher les conversations en mode fil de discussion.</li>
<li>Traductions: L'interface est en cours de traduction dans plusieurs langues.</li>
<li>Filtres: Définissez des règles pour déplacer ou copier automatiquement les e-mails dans des dossiers spécifiques, transférer ou rejeter les e-mails.</li>
<li>Dossiers: Gérez vos dossiers (ajout, suppression ou masquage).</li>
<li>Chiffrement GPG: Le client web RainLoop offre une option de chiffrement intégré.</li>
<li>Contacts: vous pouvez ajouter vos contacts et même directement lier vos contacts Nextcloud.</li>
</ul>

**ATTENTION**: *Veuillez noter qu'il s'agit d'un chiffrement côté serveur, ce qui signifie que vous n'avez aucun contrôle sur votre clé secrète. Ce n'est pas aussi sûr que d'utiliser un client de messagerie électronique de bureau, tel que Thunderbird, avec un chiffrement local. Mais encore une fois, c'est mieux que de ne pas utiliser de chiffrement du tout...*

Page d'Accueil du Projet: [https://www.rainloop.net](https://www.rainloop.net/)
