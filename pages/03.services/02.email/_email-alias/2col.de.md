---
title: 'Email-Alias'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Email Aliase<br> und Domain Linking

<a class="button button1" href="https://disroot.org/de/forms/alias-request-form">Aliase beantragen</a>
<a class="button button2" href="https://disroot.org/de/forms/domain-linking-form">Eigene Domain verlinken</a>

---


Ein Email-Alias ist im Prinzip nichts anderes als eine Art Nachsendeadresse für Deine Emails.

Mehr Aliase sind verfügbar für regelmäßige Unterstützer. Mit regelmäßigen Unterstützern meinen wir die Community-Mitglieder, die uns mindestens eine Tasse Kaffee im Monat "kaufen".

Es ist jetzt nicht so, dass wir hier Kaffee anpreisen wollen. Auf der anderen Seite ist Kaffee jedoch eine sehr griffige Analogie für Ausbeutung und [Ungleichheit](http://www.foodispower.org/coffee/)). Wir denken, dies ist eine gute Möglichkeit, jeden selbst bestimmen zu lassen, wie viel er geben kann.
Wir haben diese [Liste](https://www.numbeo.com/cost-of-living/prices_by_country.jsp?itemId=114&displayCurrency=EUR) gefunden mit Kaffeepreisen rund um die Welt. Die Liste ist vielleicht nicht hundertprozentig korrekt oder gar tagesaktuell, aber Du hast in ihr einen guten Indikator für die verschiedenen Preise. Nimm Dir ruhig Zeit, die Höhe Deines Beitrags abzuwägen. Wenn Du uns einen **Rio de Janeiro**-Kaffee im Monat "kaufen" kannst, ist das OK. Wenn Du Dir jedoch einen *doppelten entkoffeinierten Soja Frappuccino mit einem extra Shot und Sahne* im Monat leisten kannst, dann hilfst Du uns wirklich, die **Disroot**-Plattform am Laufen zu halten und sicherzugehen, dass sie frei zugänglich bleibt für andere Menschen mit weniger Mitteln.

Um ein Alias zu beantragen, fülle bitte dieses [Formular](https://disroot.org/de/forms/alias-request-form) aus.<br>
Wenn Du Deine eigene Domain nutzen willst, brauchst Du dieses [Formular](https://disroot.org/de/forms/domain-linking-form).

Für weitere Informationen, z.B. wie Du Deine neuen Email-Aliase einrichtest, schau Dir unsere [Tutorials](https://howto.disroot.org/de/tutorials/email/alias) an.
