---
title: 'Email Alias'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Alias de correo<br> y Vinculación de Dominios

<a class="button button1" href="https://disroot.org/es/forms/alias-request-form">Solicitar Alias</a>
<a class="button button2" href="https://disroot.org/es/forms/domain-linking-form">Vincular dominio propio</a>

---

<br>
Un alias de correo es simplemente una dirección de reenvío.

Hay más alias de correo disponibles para nuestrxs aportantes habituales. Con "aportantes habituales" pensamos en aquellas personas que contribuyen con el equivalente a, por lo menos, una taza de café al mes.
No estamos promoviendo el café, al contrario, pensamos que es, de hecho, un claro ejemplo de lo que es la explotación y la [inequidad](http://www.foodispower.org/coffee/), pero entendemos que es mejor dejar a la gente mensurar por sí misma cuánto pueden dar.
Encontramos esta [lista](https://www.numbeo.com/cost-of-living/prices_by_country.jsp?itemId=114&displayCurrency=EUR) de precios de tazas de café alrededor del mundo, podría no ser muy precisa, pero da un buen indicio de los diferentes costos.
Por favor, tómate el tiempo para considerar tu contribución. Si puedes 'comprarnos' una taza de café al mes en Río De Janeiro, eso está muy bien. Pero si puedes permitirte pagar un _Café Doble Descafeinado Con Crema extra_ al mes, entonces puedes ayudarnos en serio a mantener funcionando la plataforma **Disroot** y asegurar que esté disponible de manera gratuita para otras personas con menos recursos.

Para solicitar Alias necesitas completar este [formulario](https://disroot.org/es/forms/alias-request-form).<br>
Si deseas utilizar tu propio dominio, usa este [formulario](https://disroot.org/es/forms/domain-linking-form).

Para más información sobre cómo configurar tu nuevo alias de correo, mira en nuestra [sección de manuales](https://howto.disroot.org/es/tutorials/email/alias)
