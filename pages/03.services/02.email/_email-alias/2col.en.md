---
title: 'Email Alias'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Email Aliases<br> and Domain Linking

<a class="button button1" href="https://disroot.org/en/forms/alias-request-form">Request Aliases</a>
<a class="button button2" href="https://disroot.org/en/forms/domain-linking-form">Link Own Domain</a>

---

<br>
An email alias is simply a forwarding email address.

More aliases are available for our regular supporters. By regular supporters we think of those who contribute the equivalent of at least one cup of coffee a month. It is not that we are promoting coffee, on the contrary - coffee is actually quite the symbol for exploitation and [inequality](http://www.foodispower.org/coffee/) so we thought that it is a good way to let people measure themselves how much they can give.
We found this [list of coffee cup prices](https://www.numbeo.com/cost-of-living/prices_by_country.jsp?itemId=114&displayCurrency=EUR) around the world, it might not be very accurate, but it gives a good indication of the different fares. Please take time to consider your contribution. If you can 'buy' us one cup of Rio De Janeiro coffee a month that's OK, but If you can afford a Double Decaf Soy Frappuccino With An Extra Shot And Cream a month, then you can really help us keep the **Disroot** platform running and make sure it is available for free for other people with less means.


To request aliases you need to fill in this [form](https://disroot.org/en/forms/alias-request-form).<br>
If you would like to use your own domain use this [form](https://disroot.org/en/forms/domain-linking-form).

For information on howto to setup your new email aliases check our [howto section](https://howto.disroot.org/en/tutorials/email/alias).
