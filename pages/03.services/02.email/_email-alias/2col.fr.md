---
title: "Alias d'email"
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Alias d'email et liaison de domaine

<a class="button button1" href="https://disroot.org/fr/forms/alias-request-form">Demande d'alias</a>
<a class="button button2" href="https://disroot.org/fr/forms/domain-linking-form">Lier votre propre domaine</a>

---

<br>
Des alias sont simplement des adresses emails de transfert, utilisées par exemple pour simplifier une adresse mail compliquée.

Des alias supplémentaires sont disponibles pour nos contributeurs réguliers. Par contributeurs réguliers, nous pensons à ceux qui contribuent l'équivalent d'au moins une tasse de café par mois.
Ce n'est pas que nous faisons la promotion du café, bien au contraire - le café est en fait tout à fait le symbole de l'exploitation et de  [l'inégalité](http://www.foodispower.org/coffee/) donc nous avons pensé que c'était une bonne façon de laisser les gens mesurer eux-mêmes combien ils peuvent donner.
Nous avons trouvé cette [liste](https://www.numbeo.com/cost-of-living/prices_by_country.jsp?itemId=114&displayCurrency=EUR) des prix des tasses de café à travers le monde, elle n'est peut-être pas très précise, mais elle donne une bonne indication des différents tarifs.
Veuillez prendre le temps de réfléchir à votre contribution. Si vous pouvez nous "acheter" une tasse de café Rio De Janeiro par mois, c'est correct, mais si vous avez les moyens d'acheter un double décaféiné Frappuccino au soja avec un extra Shot And Cream par mois, alors vous pouvez vraiment nous aider à maintenir la plateforme **Disroot** en fonctionnement et vous assurer qu'elle est disponible gratuitement pour d'autres personnes avec moins de moyens.

Pour demander des alias vous devez remplir ce [formulaire](https://disroot.org/fr/forms/alias-request-form).<br>
Si vous souhaitez utiliser votre propre domaine, utilisez ce [formulaire](https://disroot.org/fr/forms/domain-linking-form).

*Si vous avez déjà fait un don pour **Disroot** dans le passé et que vous souhaitez toujours obtenir des alias, veuillez nous contacter.*

Pour plus d'informations sur la configuration de vos nouveaux alias de courriel, consultez notre [section de guides](https://howto.disroot.org/fr/tutorials/email/alias)
