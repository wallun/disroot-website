---
title: 'Email Alias'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Email Alias<br> e collegamento a domini

<a class="button button1" href="https://disroot.org/en/forms/alias-request-form">Richiedi un alias</a>
<a class="button button2" href="https://disroot.org/en/forms/domain-linking-form">Collega il tuo dominio</a>

---

<br>

Gli alias sono disponbili per gli utenti regolari. Con regolari si intende coloro che contribuiscono l'equivalente di almeno una tazza di caffé al mese.

Non stiamo promuovendo il caffé, al contrario, il caffé simbolizza lo sfruttamento e l'[inuguaglianza](http://www.foodispower.org/coffee/), crediamo però che sia un buon modo di permettere agli utenti di regolarsi nel contributo a **Disroot**.

Abbiamo trovato [la lista dei prezzi](https://www.numbeo.com/cost-of-living/prices_by_country.jsp?itemId=114&displayCurrency=EUR) di una tazza di caffé nel mondo. Potrebbe non essere molto accurata ma da una buona indicazione sulla differenza di prezzi.

Ti chiediamo quindi di valutare quanto donare. Se ti puoi permettere una tazza di caffé a Rio De Janeiro al mese, per noi va bene, ma se ti puoi permettere un Frapuccino doppio decaffeinato di soia con doppia panna al mese, sappi che in questo modo potresti aiutarci in modo importante a far funzionare la piattaforma Disroot ed assicurare il buon funzionamento del progetto anche per chi non ha le tue stesse disponibilità economiche.

Per richiedere un alias compila questa [form](https://disroot.org/en/forms/alias-request-form).<br>
Se invece desideri utilizzare un tuo dominio compila questa [form](https://disroot.org/en/forms/domain-linking-form).

Nel caso avessi già donato in passato a **Disroot** e ti piacerebbe comunque avere degli alias contattaci direttamente.

Per informazioni su come impostare il tuo nuovo alias guarda la sezione [guide](https://howto.disroot.org/it/tutorials/email/alias)
