---
title: 'Extra mailbox Storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Add storage to your mailbox

It is possible to extend your E-mail storage from 1GB to 10GB for the cost of 0.15 euro per GB per month, paid yearly.

To request extra storage you need to fill in this [form](https://disroot.org/en/forms/extra-storage-mailbox).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/en/forms/extra-storage-mailbox">Request Extra Mailbox Storage</a>
