---
title: 'Extra mailbox Storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Добавить дисковое пространство в почту

Вы можете расширить хранилище электронной почты с 1 ГБ до 10 ГБ по цене 0,15 евро за гигабайт в месяц при оплате за год.

Чтобы запросить дополнительное пространство для хранения, вам необходимо заполнить эту [форму](https://disroot.org/ru/forms/extra-storage-mailbox).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/en/forms/extra-storage-mailbox">Запросить дополнительное хранилище для почтового ящика</a>
