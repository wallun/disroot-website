---
title: 'Zusätzlicher Mailboxspeicher'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---


Wenngleich eine Postfachgröße von 1 GB nicht so klein ist, kann es natürlich sein, dass Du mehr Speicher benötigst. Es ist daher möglich, Deinen Email-Speicher von 1 GB auf bis zu 10 GB zu erweitern für monatlich 0,15 €/GB, jährlich zu zahlen.

Um zusätzlichen Speicher zu beantragen, fülle dieses [Formular](https://disroot.org/de/forms/extra-storage-mailbox) aus.<br>


---

## Erweitere den Speicher Deines Postfachs
<br>

<a class="button button1" href="https://disroot.org/de/forms/extra-storage-mailbox">Zusätzlichen Email-Speicher beantragen</a>
