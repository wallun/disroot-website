---
title: 'Email Clients'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: Thunderbird
        logo: thunderbird.png
        link: https://www.thunderbird.net/
        text:
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: Evolution
        logo: evolution.png
        link: https://wiki.gnome.org/Apps/Evolution
        text:
        platforms: [fa-linux]

    -
        title: KMail
        logo: kmail.png
        link: https://userbase.kde.org/KMail#Download
        text:
        platforms: [fa-linux]

    -
        title: ClawsMail
        logo: clawsmail.png
        link: https://www.claws-mail.org/downloads.php
        text:
        platforms: [fa-linux, fa-windows]

    -
        title: Mailpile
        logo: mailpile.png
        link: https://www.mailpile.is/
        text:
        platforms: [fa-linux]

    -
        title: K9Mail
        logo: k9mail.png
        link: https://k9mail.github.io/
        text:
        platforms: [fa-android]

    -
        title: FairEmail
        logo: fairemail.png
        link: https://email.faircode.eu/
        text:
        platforms: [fa-android]

    -
        title: iOS Mail App
        logo: iosmail.png
        link: https://support.apple.com/de-de/HT201320
        text:
        platforms: [fa-apple]


---

<div class=clients markdown=1>

</div>
