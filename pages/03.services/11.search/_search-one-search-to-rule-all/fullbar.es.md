---
title: 'Regla'
bgcolor: '#8EB726'
fontcolor: '#FFF'
text_align: center
---

## ¡Disroot no registra tu dirección IP!
*Usa el botón en la parte superior de la página para instalar el complemento de búsqueda de Disroot en tu Firefox.*
