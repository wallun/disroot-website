---
title: 'Crittografia su Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/NC_encryption.png?lightbox=1024)

---

## Crittografia:

Tutti i file che sono salvati sul cloud di **Disroot** sono crittati con delle chiavi generati dalla tua password. Nessuno, nemmeno gli amministratori, può vedere i contenuti dei tuoi file. Gli amministratori possono solo vedere il nome, la grandezza e il tipo di file (video, file di testo ecc.).

###### <span style="color:red">!!! Attenzione !!!</span>
**Nel caso dovessi perdere la password non sarai più in grado di recuperare i tuoi file, a meno che tu abbia impostato un sistema di recovery presente nelle impostazioni personali. Questo però significa teoricamente che tu dai il permesso agli amministratori di accedere ai tuoi file. Il modo più sicuro è di mai perdere la tua password e di avere sempre i tuoi file salvati in un backup.**

*Maggiori infomrazioni [qui](https://howto.disroot.org/it/tutorials/user/account/ussc) tutorial.*
