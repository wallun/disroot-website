---
title: 'Almacenamiento en la Nube'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Almacenamiento en la Nube

Con tu cuenta de **Disroot**, obtienes 2GB GRATIS de almacenamiento. Es posible ampliarlo a 14, 29 o 54 GB a 0.15 euro mensual por GB. También puedes elegir utilizar algo de todo el espacio adicional para almacenar correos. Por favor, especifica eso en la sección de comentarios en el formulario de solicitud.

Para solicitar almacenamiento extra necesitas completar este [formulario](https://disroot.org/es/forms/extra-storage-space).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/es/forms/extra-storage-space">Solicitar almacenamiento extra</a>
