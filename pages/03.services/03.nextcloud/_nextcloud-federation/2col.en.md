---
title: 'Nextcloud Federation'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Federation

Thanks to federation you can link and share your folders with anyone using other **Nextcloud** (or **ownCloud**) instances other than **Disroot**.

---

![](en/NC_federation.png?lightbox=1024)
