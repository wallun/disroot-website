---
title: 'Extremo a Extremo'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/endtoend-android-nw.png?lightbox=1024)

---

## Cifrado de extremo a extremo

El [**cifrado de extremo a extremo**](https://es.wikipedia.org/wiki/Cifrado_de_extremo_a_extremo) de **Nextcloud** ofrece la máxima protección para tu información, permitiendo a los usuarios y usuarias, a través del plugin de cifrado, escoger una o más carpetas en su cliente de escritorio o móvil para cifrar de esta manera. Las carpetas pueden ser compartidas con otrxs usuarixs y sincronizadas entre dispositivos pero no son legibles para el servidor.

Read more about it at [https://nextcloud.com/endtoend/](https://nextcloud.com/endtoend/)

<span style="color:red">**¡El cifrado de Extremo-a-extremo todavía se encuentra en fase alfa, no lo uses en producción y solo con datos de prueba!**</span>

### ¡ATENCIÓN!

Actualmente, el cifrado de extremo-a-extremo de Nextcloud está deshabilitado en Disroot. Esto es debido a un problema de larga data con la aplicación de escritorio de Nextcloud.

Para leer más (en inglés) sobre estos problemas:

[Can't revoke keys](https://github.com/nextcloud/end_to_end_encryption/issues/32)

[Directory structure is leaked](https://github.com/nextcloud/end_to_end_encryption/issues/94)

[Uploding e2e encrypted files](https://github.com/nextcloud/desktop/issues/890)
