---
title: 'Nextcloud Text'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Text

Create markdown files, and directly edit them online with a live preview. You can also share them and make them editable to anyone.

---

![](en/nextcloud-text.png?lightbox=1024)
