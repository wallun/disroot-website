---
title: 'News'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Scegli il tuo client preferito'
clients:
    -
        title: Feedreader
        logo: en/rss_logo.png
        link: https://github.com/jangernert/FeedReader
        text:
        platforms: [fa-linux]
    -
        title: News
        logo: en/news_logo.png
        link: https://f-droid.org/en/packages/de.luhmer.owncloudnewsreader/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-news.png?lightbox=1024)

---

## News

Aggregatore di feed RSS/Atom. Puoi tenere traccia e organizzare tutti i tuoi feed in un unico posto e sincronizzarli su più device.
