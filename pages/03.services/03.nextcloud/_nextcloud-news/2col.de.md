---
title: 'Nachrichten'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Wähle Deinen bevorzugten Client'
clients:
    -
        title: Feedreader
        logo: de/rss_logo.png
        link: https://github.com/jangernert/FeedReader
        text:
        platforms: [fa-linux]
    -
        title: News
        logo: de/news_logo.png
        link: https://f-droid.org/de/packages/de.luhmer.owncloudnewsreader/
        text:
        platforms: [fa-android]
---

![](de/nextcloud-news.png?lightbox=1024)

---

## Nachrichten

RSS/Atom Feed-Portal. Du kannst all Deine Feeds an einem Ort verfolgen, verwalten und zwischen verschiedenen Geräten synchronisieren.
