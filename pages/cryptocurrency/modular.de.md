---
title: Kryptowährungen
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _crypto
            - _bitcoin
            - _monero
            - _faircoin
            - _bottompage
body_classes: modular
header_image: burning_money.png
---
