---
title: 'Disroot App'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: right
---

<br>

## Consíguela en [![F-droid](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/es/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/es/packages/org.disroot.disrootapp/?class=lighter)

---

## La App de Disroot
Es como tu navaja suiza de la plataforma, hecha por la comunidad para la comunidad. Si no tienes una cuenta de Disroot aún puedes utilizar esta aplicación para acceder a los servicios que no requieren una, como los Blocs, Subida, etc.
