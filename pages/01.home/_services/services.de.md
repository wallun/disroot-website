---
title: Services
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Email
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Freie und sichere Email-Accounts, nutzbar mit einem IMAP-Client oder via Online-Benutzeroberfläche."
        button: 'https://mail.disroot.org'
        buttontext: "Anmelden"
    -
        title: Cloud
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "Deine Daten unter Deiner Kontrolle! Sichern, Synchronisieren, Teilen von Kalendern, Kontakten und mehr."
        button: 'https://cloud.disroot.org'
        buttontext: "Anmelden"
    -
        title: Forum
        icon: forum.png
        link: 'https://disroot.org/services/forum'
        text: "Diskussionsforen / Mailinglisten für Deine Projekte, Gruppen oder Gemeinschaften."
        button: 'https://forum.disroot.org'
        buttontext: "Anmelden"
    -
        title: XMPP-Chat
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Dezentralisiertes, sicheres und freies Instant-Messaging."
        button: 'https://webchat.disroot.org'
        buttontext: "Anmelden"
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Gemeinschaftlich und in Echtzeit Dokumente direkt im Webbrowser bearbeiten"
        button: 'https://pad.disroot.org'
        buttontext: "Ein Pad starten"
    -
        title: Calc
        icon: calc.png
        link: 'https://disroot.org/services/pads'
        text: "Gemeinschaftlich und in Echtzeit Tabellen direkt im Webbrowser bearbeiten"
        button: 'https://calc.disroot.org'
        buttontext: "Ein Calc starten"
    -
        title: 'Paste-Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Minimalistischer, quelloffener und verschlüsselter Wegwerf-Container (Paste-Bin) mit Diskussionsboard."
        button: 'https://bin.disroot.org'
        buttontext: "Teile einen Paste-Bin"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Software zur verschlüsselten, temporären Dateiaufbewahrung und -verbreitung."
        button: 'https://upload.disroot.org'
        buttontext: "Datei teilen"
    -
        title: Suche
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Eine anonyme Meta-Suchmaschine."
        button: 'https://search.disroot.org'
        buttontext: "Suche"
    -
        title: Umfragen
        icon: polls.png
        link: 'https://disroot.org/services/polls'
        text: "Onlinedienst zur Abstimmung von Terminen oder um schnell eine gemeinsame Entscheidung zu treffen."
        button: 'https://poll.disroot.org'
        buttontext: "Umfrage starten"
    -
        title: Projekt-Board
        icon: project_board.png
        link: 'https://disroot.org/services/project-board'
        text: "Ein Werkzeug zur Projektverwaltung im Kanban-Stil."
        button: 'https://board.disroot.org'
        buttontext: "Anmelden"
    -
        title: 'Calls'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Ein Werkzeug für Videokonferenzen."
        button: 'https://calls.disroot.org'
        buttontext: "Aufruf"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "A code hosting and project collaboration."
        button: 'https://git.disroot.org'
        buttontext: "Log in"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "A low latency, high quality voice chat application."
        button: 'https://mumble.disroot.org'
        buttontext: "Log in"
        #badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "A private-by-design alternative to popular office tools."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Access"
        #badge: EXPERIMENTAL
---
