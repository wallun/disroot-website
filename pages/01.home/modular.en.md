---
title: Disroot
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _about
            - _donatebutton
            - _donation
            - _signup
            - _services
            - _disapp
menu: home
big_header: true
onpage_menu: true
---
