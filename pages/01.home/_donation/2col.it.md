---
title: Online-donation
bgcolor: '#fff'
wider_column: left
fontcolor: '#1e5a5e'
---

![](premium.png?class=reward) Disroot si basa sulle donazioni e sul sostegno della sua comunità e degli utenti dei servizi. Se desideri mantenere attivo il progetto e contribuire a creare spazio per potenziali nuovi disrooter, utilizza uno dei metodi disponibili per fornire un contributo finanziario. [Alias e domini personalizzati](/services/email#alias) sono disponibili per donatori regolari.

Puoi anche contribuire acquistando [spazio aggiuntivo per l'email](/services/email#storage) oppure [spazio aggiuntivo per il cloud](/services/nextcloud#storage).

---

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Dona utilizzando Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Diventa un Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://flattr.com/profile/disroot" target=_blank><img alt="Flatter" src="donate/_donate/f_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://disroot.org/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

<span style="color:#4e1e2d; font-size:1.1em; font-weight:bold;">Coordinate bancarie:</span>
<span style="color:#4e1e2d; font-size:1.1em;">
Stichting Disroot.org
IBAN NL19 TRIO 0338 7622 05
BIC TRIONL2U
</span>
