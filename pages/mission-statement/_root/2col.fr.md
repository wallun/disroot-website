---
title: 'Déclaration de mission'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

## <br><br>
<span style="float:right;">
![](disroot-root.png)
</span>

---

## Mission de la fondation Disroot.org

<br>
La mission fixée par la fondation Disroot.org vise à récupérer l'Internet comme l'outil diversifié et indépendant qu'il a était censé être.

Internet, autrefois décentralisé, démocratique et libre, a été dominé par une poignée de géants de la technologie, ce qui a favorisé la concentration dans des monopoles, un contrôle gouvernemental accru et une réglementation plus restrictive. Tout ce qui, à notre avis, s'oppose et détruit l'essence même de ce merveilleux outil.

En encourageant l'utilisation de logiciels libres et open source, nous soutenons l'effort collectif et communautaire vers la collaboration. Disroot.org veut être un exemple pour montrer qu'il existe des alternatives possibles au monde de l'entreprise qui est proposé dans son ensemble. Des alternatives qui ne sont pas construites sur la base de l'obtention d'avantages économiques. Des alternatives pour lesquelles la transparence, l'ouverture, la tolérance et le respect sont des éléments clés.

### La plate-forme

Disroot.org offre une plate-forme de services Web fondée sur les principes de confidentialité, d'ouverture, de transparence et de liberté. Son objectif le plus important d'un point de vue technique est de fournir le meilleur service possible avec le meilleur niveau d'assistance. Nous avons choisi une approche de travail dans laquelle les utilisateurs (désormais appelés Disrooters) sont la partie la plus précieuse et les principaux bénéficiaires du projet ; les décisions sont prises en pensant à eux et non en profitant d'avantages économiques, d'épanouissement personnel, de contrôle ou de pouvoir. Disroot.org s'engage à défendre ces principes clés de la meilleure manière possible.

La vie privée en ligne est constamment compromise par ceux qui recherchent le profit et le contrôle financier. Nos données sont devenues un bien très précieux et la plupart des entreprises en ligne en profitent ouvertement ou subrepticement. Disroot.org s'engage à ne jamais vendre, traiter ou utiliser les informations fournies par Disrooters à des tiers à des fins financières, politiques ou de prise de pouvoir. Les données de Disrooters leur appartiennent et le projet les stocke simplement pour que ces Disrooters puissent les utiliser. Notre devise est "Moins nous en savons sur nos utilisateurs, mieux c'est". Nous mettons en œuvre le chiffrement des données chaque fois que cela est possible pour nous assurer que l'obtention des données des utilisateurs par des tiers non autorisés est aussi difficile que possible et nous ne conservons que le minimum de journaux/logs des utilisateurs et des données qui sont essentielles pour la performance du service.
