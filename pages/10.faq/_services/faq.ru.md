---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "СЕРВИСЫ"
section_number: "400"
faq:
    -
        question: "Какие сервисы я могу использовать с моей учетной записью Disroot?"
        answer: "<p>Ваша учетная запись позволяет вам использовать следующие службы с тем же именем пользователя и паролем:</p>
        <ul class=disc>
          <li>Почта</li>
          <li>Облако</li>
          <li>Форум</li>
          <li>Чат</li>
          <li>Доска проектов</li>
        </ul>
        <p>Остальные услуги (<b>Pads, PasteBin, Upload, Search, Polls</b>) не требуют регистрации.</p>"
    -
        question: "Что я могу использовать для своей учетной записи электронной почты?"
        answer: "<p>Вы можете использовать его как хотите, кроме коммерческих целей или для рассылки спама. Чтобы лучше понять, что вы можете и чего не можете с этим делать, прочитайте параграфы 10 и 11 наших <a href='https://disroot.org/en/tos' target='_blank'>правил</a>.</p>"
    -
        question: "Каков размер почтового ящика и облака?"
        answer: "<p><b>Размер почтового хранилища</b> ограничен <b>1GB</b> и <b>размером вложений</b> до <b>50MB</b>.</p>
        <p><b>Дисковое пространство облака</b> по умолчанию равно <b>2GB</b>.</p>
        <p>Есть возможность расширить ваше облако и почтовое хранилище. Посмотрите варианты <a href='https://disroot.org/en/forms/extra-storage-space/' target='_blank'>здесь</a></p>"
    -
        question: "Могу ли я увидеть статус сервисов, например, время безотказной работы, плановое обслуживание и т.д.?"
        answer: "<p>Да. Есть несколько способов быть в курсе всех проблем, перерывов на обслуживание и общей информации о состоянии здоровья платформы.</p>
        <ul class=disc>
        <li>Посетите <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Подпишитесь на RSS ленту <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Подпишитесь на почтовую рассылку <a href='https://state.disroot.org' target='_blank'>https://state.disroot.org</a></li>
        <li>Следите за <b>disroot_state@hub.disroot.org</b> в Hubzilla, Diaspora*, Mastodon, Pleroma, Pixelfed, etc. (the fediverse...)</li>
        <li>Читайте <b>disroot@social.weho.st</b> (the fediverse again...)</li>
        <li>Присоединяйтесь <b>state@chat.disroot.org</b> (XMPP)</li>
        <li>Присоединяйтесь <b>@state:disroot.org</b> (Matrix)</li>
        <li>Установите <b>DisApp</b> - <b>Disroot</b> приложение для Android с уведомлениями в реальном времени.</li>
        </ul>"
---
