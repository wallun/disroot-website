---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "KOMMUNIKATION"
section_number: "100"
faq:
    -
        question: "Wie kann ich Disroot's IRC in XMPP-Räumen nutzen?"
        answer: "<p><b>Disroot</b> bietet ein sogenanntes IRC-Gateway. Das heißt, Du kannst jeden IRC-Raum auf jedem IRC-Server betreten. Das grundsätzliche Adress-Schema sieht folgendermaßen aus:</p>
        <ol>
        <li>Um einen IRC-Raum zu betreten: <b>#Raum%irc.domain.tld@irc.disroot.org</b></li>
        <li>Um einen IRC-Kontakt hinzuzufügen: <b>Kontaktname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Dabei ist <b>#Raum</b> der IRC-Raum, den Du betreten willst, und <b>irc.domain.tld</b> ist die IRC-Serveradresse, auf der der Raum gehostet wird. Achte darauf, dass Deine Adresse <b>@irc.disroot.org</b> enthält, da dies die Adresse des IRC-Gateways ist.</p>"

---
