---
title: Contacto
bgcolor: '#1F5C60'
fontcolor: '#FFF'
process:
    markdown: true
    twig: true
twig_first: false
---

[![](email.png?classes=contact)](mailto:support@disroot.org) support@disroot.org

[![](email.png?classes=contact,pull-left)](mailto:cryptsupport@disroot.org) <div> <br/> cryptsupport@disroot.org <br/> GPG: [keyserver.ubuntu.com](https://keyserver.ubuntu.com/pks/lookup?search=cryptsupport%40disroot.org&fingerprint=on&op=index) <br/> Huella digital: FFC9 DCE6 5A96 98B0 FE49  EA83 6734 AE05 298C 41E8 </div>

[![](mastodon.png?classes=contact)](https://social.weho.st/@disroot) Mastodon: https://social.weho.st/@disroot

[![](hubzilla.png?classes=contact)](https://hub.disroot.org/channel/disroot) Hubzilla: https://hub.disroot.org/channel/disroot

---

![](irc.png?classes=contact) IRC: #disroot sobre irc.freenode.net

[![](xmpp.png?classes=contact)](xmpp:disroot@chat.disroot.org?join) XMPP: [disroot@chat.disroot.org](xmpp:disroot@chat.disroot.org?join)

[![](webchat.png?classes=contact)](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org) [Webchat XMPP](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org)

[![](nextcloud.png?classes=contact)](https://cloud.disroot.org/call/di5y9zno
) NextCloud: https://cloud.disroot.org/call/di5y9zno


---

All listed chatrooms are bridged together. This means that no matter which chatroom from this list you choose, you will be able to communicate with all the comunity via a chat relay bot.
